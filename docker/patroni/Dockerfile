FROM postgres:10
MAINTAINER Emanuel Calvo <3manuek@gmail.com>

RUN export DEBIAN_FRONTEND=noninteractive \
    && echo 'APT::Install-Recommends "0";\nAPT::Install-Suggests "0";' > /etc/apt/apt.conf.d/01norecommend \
    && apt-get update -y \
    && apt-get upgrade -y

RUN apt-get install -y curl jq git locales python3-pip python3-setuptools python3-wheel python3-psutil \
                            python3-requests python3-six python3-psycopg2 locales \
    && localedef -i en_US -c -f UTF-8 -A /usr/share/locale/locale.alias en_US.UTF-8
    ## Make sure we have a en_US.UTF-8 locale available

RUN pip3 install --upgrade pip

RUN pip3 install psycopg2-binary requests click tzlocal cdiff pysocks prettytable six pyyaml \
    && pip3 install patroni[consul] \
    && mkdir -p /home/postgres \
    && chown postgres:postgres /home/postgres 

RUN apt-get remove -y python3-pip python3-setuptools \
    && apt-get purge -y --auto-remove \
    && rm -rf /var/lib/apt/lists/* /root/.cache

ADD ./entrypoint.sh /
ADD ./post_init.sh /

### Setting up a simple script that will serve as an entrypoint
RUN mkdir /data/ && touch /.pgpass /patroni.yml \
    && chown postgres:postgres -R /data/ /.pgpass /patroni.yml /var/run/ /var/lib/ /var/log/

# Expose volume for persistency
VOLUME /data  

# Postgres Port
EXPOSE 5432
# Patroni HTTP API
EXPOSE 8008 

ENV LC_ALL=en_US.UTF-8 LANG=en_US.UTF-8
ENTRYPOINT ["/bin/bash", "/entrypoint.sh"]
USER postgres

