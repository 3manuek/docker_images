#!/bin/sh

ODYSSEY_CONF=${CUSTOM_ODYSSEY_CONF:-$1}

/etc/init.d/syslog-ng start

exec /usr/local/bin/odyssey ${ODYSSEY_CONF} && tail -f /var/log/odyssey.log


