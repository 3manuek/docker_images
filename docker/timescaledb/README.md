## Timescale

References and links:

- [Timescale Docker](https://github.com/timescale/timescaledb-docker)
- [Timescale Docker with Replication](https://github.com/timescale/streaming-replication-docker/blob/master/stack.yml)
- [Timescale Image Hub](https://hub.docker.com/r/timescale/timescaledb/)
- Sample dataset included from [Sample Dataset](https://docs.timescale.com/v1.0/tutorials/other-sample-datasets)


```
tar -xvzf devices_small.tar.gz

psql -U postgres -hlocalhost -p6666 -c "CREATE DATABASE devices_small"

psql -U postgres -hlocalhost -d devices_small -p6666  < devices.sql

# (3) import data from .csv files to the database
psql -U postgres -hlocalhost -d devices_small -p6666 -c "\COPY readings FROM devices_small_readings.csv CSV"
psql -U postgres -hlocalhost -d devices_small -p6666 -c "\COPY device_info FROM devices_small_device_info.csv CSV"
```
