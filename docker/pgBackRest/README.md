## Warning
This image is WIP and not tested neither working due the following factors:

- ssh for docker is kinda buggy and may not be recommended at all
- This image uses Centos, ideally we should stick to debian family.

## Pgbackrest
### Install pgbackrest on both nodes
#### Configuration:

##### Principal server
Add the IP of the destination server in the **db-host = IP** parameter of the **pgbackrest_ongres.conf** file

##### On the destination server

  In the file **/etc/pgbackrest.conf** add
```
  [global]
backup-host=backup-host
backup-user=postgres
backup-ssh-port=23
log-level-file=detail

[db-primary]
db-path=/var/lib/pgsql/10/data
```

### Docker Secrets
```
docker run -it -p 23:22  -v $HOME/.ssh/:/home/postgres/.ssh/ -v $(pwd)/data/:/var/lib/pgbackrest/ -v $(pwd)/conf/:/home/postgres/conf/ pgbackrest
```

After the docker is up, we must enable the ssh service with:
  ```nohup /sbin/sshd -D & ```  
