#!/bin/bash

DOCKER_IP=$(hostname --ip-address)
PATRONI_SCOPE=${PATRONI_SCOPE:-ongres}
DEF_PATRONI_RESTAPI_CONNECT_PORT=8008


#export PATRONI_SCOPE
export PATRONI_NAME="${PATRONI_NAME:-${HOSTNAME}}"
export PATRONI_RESTAPI_CONNECT_ADDRESS="${DOCKER_IP}:${PATRONI_RESTAPI_CONNECT_PORT:-$DEF_PATRONI_RESTAPI_CONNECT_PORT}"
export PATRONI_RESTAPI_LISTEN="0.0.0.0:${PATRONI_RESTAPI_CONNECT_PORT:-$DEF_PATRONI_RESTAPI_CONNECT_PORT}"
export PATRONI_admin_PASSWORD="${PATRONI_admin_PASSWORD:=admin}"
export PATRONI_admin_OPTIONS="${PATRONI_admin_OPTIONS:-createdb, createrole}"
export PATRONI_POSTGRESQL_CONNECT_ADDRESS="${DOCKER_IP}:5432"
export PATRONI_POSTGRESQL_LISTEN="0.0.0.0:5432"
export PATRONI_POSTGRESQL_DATA_DIR="data/${PATRONI_SCOPE}"
export PATRONI_REPLICATION_USERNAME="${PATRONI_REPLICATION_USERNAME:-replicator}"
export PATRONI_REPLICATION_PASSWORD="${PATRONI_REPLICATION_PASSWORD:-abcd}"
export PATRONI_SUPERUSER_USERNAME="${PATRONI_SUPERUSER_USERNAME:-postgres}"
export PATRONI_SUPERUSER_PASSWORD="${PATRONI_SUPERUSER_PASSWORD:-postgres}"
export PATRONI_POSTGRESQL_PGPASS="/.pgpass"


#DEFAULT_CONSUL_ARGS="agent -advertise $DOCKER_IP -retry-join \"${CONSUL_BOOTSTRAP_HOST}\" -ui -client=0.0.0.0 -bind=0.0.0.0 -datacenter=${CONSUL_DATACENTER}"
DEFAULT_CONSUL_ARGS="agent -dev -ui"
CONSUL_ARGS=${CONSUL_CUSTOM_ARGS:-DEFAULT_CONSUL_ARGS}


[ ! -s /patroni.yml ] && cat > /patroni.yml <<__EOF__
dcs_api: 'consul://${PATRONI_CONSUL_HOST}'
namespace: /service/
scope: ${PATRONI_SCOPE}
#name: ${PATRONI_NAME}

restapi:
  listen: ${PATRONI_RESTAPI_LISTEN}    # we want this to be accessed locally only

consul:
  host: ${PATRONI_CONSUL_HOST}
  port: 8500

bootstrap:
  dcs:
    ttl: 30
    loop_wait: 10
    retry_timeout: 10
    maximum_lag_on_failover: 1048576
    postgresql:
      use_pg_rewind: true

  initdb:  # Note: It needs to be a list (some options need values, others are switches)
  - encoding: UTF8
  - data-checksums

  pg_hba:
  - host all all 0.0.0.0/0 trust
  - host replication replicator ${DOCKER_IP}/16    trust
__EOF__

mkdir -p "$HOME/.config/patroni"
[ -h "$HOME/.config/patroni/patronictl.yaml" ] || ln -s /patroni.yml "$HOME/.config/patroni/patronictl.yaml"


# echo Consul args $CONSUL_ARGS # 2> /var/log/consul/consul.log
consul ${CONSUL_ARGS} &

export PATRONI_CONSUL_URL="http://127.0.0.1:8500"

#cat /var/log/consul/consul.log

[ -z $CHEAT ] && exec patroni /patroni.yml

while true; do
    sleep 60
done

