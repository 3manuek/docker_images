## OnGres Postgres (and 3rd party) Docker Images

## Warning

> These images are not intended to be production ready in the way that there are several things
> to consider regarding security (like installing gosu for easy step down to root) and TLS/SSL support. 
> Although it is a starting point up to your production image.


## Regarding composes

All the composes for iamge building have a basic setup within a basic architecture. 
For bulding the image locally, just issue:

```
cd docker/<component>
docker-compose build
docker-compose up 
```

Using `make` may fail as you may not have permissions to our organization, although you
can change both tag and image in the compose YAMLs.


## Makefile 

```
make <target>
```

This only builds, it does not "run" the image.
For running an image, enter the folder and if the image has a 
docker-compose.yml file, start with:

```
docker-compose up
```

Push images:

```
make <target>-push
```