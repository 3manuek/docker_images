IMG_DIR=./docker

ENABLED_IMAGES = timescale postgres-compiled pgbouncer-consulag pgbouncer patroni patroni-consulag odyssey


define helptext
Try 'make <image>-[build|push]'

Target all will apply to the following enabled images: $(ENABLED_IMAGES)
endef


.PHONY: %-build

%-build:
	cd $(IMG_DIR)/$* && docker-compose build 

.PHONY: all
all:
	for dir in $(ENABLED_IMAGES); do \
		cd $(IMG_DIR)/$$dir && docker-compose build &&\
		docker-compose push ; \
	done

.PHONY: %-push
%-push:
	cd $(IMG_DIR)/$* && docker-compose push 

help: 
	echo $(helptext)

list:
	ls -l $(IMG_DIR)

.PHONY: clean

clean:
	echo "Not yet implemented"
