## TODO

### Production Images

- Add SSL to all, including docker configuration

### PgBouncer

- Add support for adding custom auth_query with function (need to import schema).

### Patroni

- Add postgresql configuration in patroni.yml 

### Consul

- Add Consul image for servers


### PgBackRest

- Move to debian family
- Avoid exposing volumes with keys or .ssh


### Postgres

- Add compilation flags at docker-compose e.g `--with-llvm` or `--enable-asserts`

### PgPool

TODO image

Some resources:

https://github.com/tatsuo-ishii/docker-pgpool-II-pgpool_setup
https://github.com/paunin/PostDock


### repmgr

- Import dockerfiles from https://gitlab.com/ongresinc/testing-pg-ha-solutions/tree/master


### Other

- Implement backup configuration server container (pgBackRest, wal-e? wal-e-proxy?)
- Implement compose with all the images working together, including entrypoints. See compose/README.md

## confd 

See Odyssey, we started to experiment there (not implemented yet, only installed in the image).

Here we are working on getting confd getting keys from patroni so we update this through consul call.

https://github.com/kelseyhightower/confd/blob/master/docs/quick-start-guide.md

In Patroni they do something like this:

```python
                confd)
                    haproxy -f /etc/haproxy/haproxy.cfg -p /var/run/haproxy.pid -D
                    CONFD="confd -prefix=${PATRONI_NAMESPACE:-/service}/$PATRONI_SCOPE -interval=10 -backend"
                    if [ ! -z ${PATRONI_ZOOKEEPER_HOSTS} ]; then
                        while ! /usr/share/zookeeper/bin/zkCli.sh -server ${PATRONI_ZOOKEEPER_HOSTS} ls /; do
                            sleep 1
                        done
                        exec $CONFD zookeeper -node ${PATRONI_ZOOKEEPER_HOSTS}
                    else
                        while ! curl -s ${PATRONI_ETCD_URL}/v2/members | jq -r '.members[0].clientURLs[0]' | grep -q http; do
                            sleep 1
                        done
                        exec $CONFD etcd -node $PATRONI_ETCD_URL
                    fi
                    ;;
```

What we are going to do is to use Consul instead through watcher _remotely_, that is, spinning a consul agent and
issue the confd remotely update and reload.

For that purpose this is the way:

```bash 
confd -onetime -backend consul -node consul-agent:8500
```


