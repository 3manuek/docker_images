## What this does

TODO is write a compose implementing all them over with other images and more complex scenarios.

The example/ contains some very basic structure using 2 components, but you can also pick from `docker/<component>/docker-compose.yaml` .
Those work and some already have some reusable configuration and is all autoscaled (see that no volumes are mounted, that is on purpose
as we don't want to complex the environment).

> Do not include build property in these composes as it is not intended to produce images from here.

We need is create like different specs, integrating Consul, backups or monitoring using OnGres images.

I'm thinking a structure like the above:

```yaml
compose/
    3_node_patroni_pgbouncer/
        docker-compose.yaml
        .env 
        ...
    2_node_patroni_pbbackrest_odyssey/
    ...
```

This is necessary for testing Consul watchers and deploys through ansible for installing 
additional tools ,configuration, etc.

The configuration of the poolers is isolated into odyssey's
routing.conf and pgbouncer's entrypoint.ini. This makes easier the development of the over writting
scripts. 
