## Dual remote

This repo works with 2 remotes. dev is at https://gitlab.com/3manuek/docker_images

```
# origin here is 
git clone https://gitlab.com/3manuek/docker_images
cd docker_images
git remote add bitbucket git@bitbucket.org:ongresinc/postgres_docker_images.git
git remote update
git remote -v
git push bitbucket master
git push origin master
```

The ongres repository is considered the stable. 

You can test on your origin and push to master if you feel confortable.
Although, you can either put a PR (on any of the remotes! You can even 
add GitHub).

You can also do something like [this](https://stackoverflow.com/questions/11690709/can-a-project-have-multiple-origins/11690868):

```bash
# Push my-branch to github and set it to track github/my-branch
$ git push -u github my-branch

# Make some existing branch track github instead of origin
$ git branch --set-upstream other-branch github/other-branch
```
